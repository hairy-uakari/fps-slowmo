﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlowMotion : MonoBehaviour {

    float totalTime = 2;
    float timeElapsed = 0;
    bool toggle = false;

    public GameObject timer;
    private  Text timeLeftText;
    private  Image loading;

    void Start() {
        timeLeftText = timer.transform.GetChild(2).gameObject.GetComponent<Text>();
        loading = timer.transform.GetChild(1).gameObject.GetComponent<Image>();
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.Q)) {
            toggle = !toggle;
            timer.SetActive(toggle);
        }

        if (toggle && timeElapsed < totalTime) {
            Time.timeScale = 0.2f;
            Time.fixedDeltaTime = 0.02f * Time.timeScale;
            timeElapsed += Time.deltaTime * (1 / Time.timeScale);
            timeElapsed = Mathf.Clamp(timeElapsed, 0, totalTime);
            loading.fillAmount = (1 - timeElapsed / totalTime);
            float redness = Util.Lerp(timeElapsed / totalTime, 1, 0);
            timeLeftText.text = (totalTime - timeElapsed).ToString("n2");
            timeLeftText.color = new Color(1, redness, redness);
        }
        else {
            timer.SetActive(false);
            Time.timeScale = 1;
            Time.fixedDeltaTime = 0.02f * Time.timeScale;
            timeElapsed = 0;
            toggle = false;
            loading.fillAmount = 1;
            timeLeftText.color = new Color(1, 1, 1);
        }
    }
}
