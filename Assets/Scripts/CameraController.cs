﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public float sensitivity = 100f;
    private Rigidbody rb;
    private float xRot = 0;
    private float yRot = 0;
    void Start() {
        rb = GetComponent<Rigidbody>();
        Cursor.lockState = CursorLockMode.Locked;
    }

    void FixedUpdate() {
        float mouseX = Input.GetAxis("Mouse X") * sensitivity * Time.fixedDeltaTime,
              mouseY = Input.GetAxis("Mouse Y") * sensitivity * Time.fixedDeltaTime;
        // transform.Rotate(Vector3.up * mouseX);
        // transform.Rotate(-Vector3.right * mouseY);
        yRot += mouseX;
        xRot -= mouseY;
        xRot = Mathf.Clamp(xRot, -45, 45);
        rb.rotation = Quaternion.Euler(xRot, yRot, 0);
    }
}
