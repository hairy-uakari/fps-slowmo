﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour {

    void Start() {
        
    }

    void Update() {
        if (Input.GetAxis("Fire1") != 0) {
            RaycastHit hit;
            Vector3 parent = transform.parent.localEulerAngles;
            Vector3 current = transform.localEulerAngles;
            Vector3 pointed = new Vector3(parent.x + current.x, parent.y + current.y, 0).normalized;
            if (Physics.Raycast(transform.position, -transform.forward, out hit, 100) && 
                hit.collider.tag == "Jasper") {
                    Damage script = hit.collider.GetComponent<Damage>();
                    if (!script.isCoroutineRunning)
                        StartCoroutine(script.Hit(10));
                    else
                        StopCoroutine(script.Hit(10));
            }
        }
    }

    void OnDrawGizmos() {
        Gizmos.DrawLine(transform.position, -transform.forward * 100 + transform.position);
    }

}
