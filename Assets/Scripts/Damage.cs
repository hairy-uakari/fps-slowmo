﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage : MonoBehaviour {

    public int health = 100;
    public bool isCoroutineRunning = false;
    Renderer[] renderers;
    void Start() {
        renderers = GetComponentsInChildren<Renderer>();
    }

    void Update() {

    }

    public IEnumerator Hit(int damage) {
        health -= damage;
        if (health <= 0) {
            Destroy(gameObject);
            yield break;
        }
        isCoroutineRunning = true;
        foreach(var r in renderers)
            r.material.color = Color.red;
        yield return null;
        foreach(var r in renderers)
            r.material.color = Color.white;
        isCoroutineRunning = false;
    }
}
