﻿public class Util {
    public static float Lerp(float t, float from, float to) {
        return (1 - t) * from + t * to;
    }
}
