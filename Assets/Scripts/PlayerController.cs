﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    private Rigidbody rb;
    public float speed = 10;
    public const float jumpTime = 0.3f;
    private bool jumpedPressed = false;
    private float jumpPressedAt = 0;
    private float currentJumpForce = 130;
    private float downwardForce = 0;
    private Animator animator;
    private ParticleSystem ps;
    void Start() {
        rb = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        ps = GetComponentInChildren<ParticleSystem>();
    }

    void FixedUpdate() {
        MoveCharacter();
        HandleJump();
        Shoot();
    }

    void MoveCharacter() {
        Vector3 forward = new Vector3(transform.forward.x, 0, transform.forward.z);
        Vector3 horizontal = Input.GetAxis("Horizontal") * transform.right * speed * Time.fixedDeltaTime,
                vertical   = Input.GetAxis("Vertical") * forward * speed * Time.fixedDeltaTime;
        Vector3 total = horizontal + vertical;
        Debug.Log(total);
        rb.MovePosition(
            transform.position + total
        );
    }

    void HandleJump() {
        float jumped = Input.GetAxis("Jump");
        if (!jumpedPressed && jumped != 0)
            jumpedPressed = true;
        if (jumpedPressed) {
            float forceToBeApplied = jumpPressedAt / jumpTime;
            Debug.Log(forceToBeApplied);
            if (jumpPressedAt < jumpTime) {
                currentJumpForce = Mathf.Lerp(currentJumpForce, 0, forceToBeApplied);
                rb.AddForce(new Vector3(0, currentJumpForce, 0));
                jumpPressedAt += Time.fixedDeltaTime;
            }
            downwardForce = Mathf.Lerp(0, 25, forceToBeApplied);
            rb.AddForce(0, -downwardForce, 0);
        }
    }

    void OnCollisionEnter(Collision coll) {
        jumpedPressed = false;
        currentJumpForce = 130;
        jumpPressedAt = 0;
    }

    void Shoot() {
        float shoot = Input.GetAxis("Fire1");
        if (shoot != 0) {
            animator.SetBool("shooting", true);
            ps.Play();
        }
        else {
            animator.SetBool("shooting", false);
            ps.Stop();
        }
    }

}
